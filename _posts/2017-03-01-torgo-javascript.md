---
layout: post
title: Torgo in a Web-Browser
date: '2017-03-01 12:07:17 -0500'
categories: dev
tags: [dev, javascript]
author: Matthew Aguirre
---

Coming soon, a Web-Browser enabled version of [Torgo]({{site.url}}/torgo-javascript/)!  
Also in development, a [node/electron version](http://github.com/ZenHarbinger/torgo-nodejs).
